#include "TestLib/TestClass.h"
#include <string>
#include <iostream>

TestClass::TestClass():testString("default") {
	std::cout<<"this is the default constructor "<<std::endl;
}

TestClass::TestClass(std::string t):testString(t) {
	std::cout<<"this is the string constructor "<<t<<std::endl;
}

void TestClass::Print() const {
	std::cout<<" this is Print() : "<<testString<<std::endl;
}
