#pragma once

#include <string>

class TestClass {
public:
	TestClass();
	TestClass(std::string);
	void Print() const;
private:
	std::string testString;
};
