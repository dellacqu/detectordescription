get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/DetectorDescription-ExpressionEvaluator.cmake)
include(${SELF_DIR}/DetectorDescription-TestLib.cmake)
include(${SELF_DIR}/DetectorDescription-JSONParser.cmake)
include(${SELF_DIR}/DetectorDescription-XMLParser.cmake)

